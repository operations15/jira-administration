
/*********************************************************************************************************************
* timeRollup.groovy
* v1.1
* - Updated to include Rough Estimate Roll-up To Epics
*
* ScriptRunner Type: Post Function Transition
*
* This script monitors Original Estimates, Remaining Estimates, and Time Spent (Work logged) on Tasks and WIP-Defects. 
* Updates to these metrics on a Task or WIP-Defect, results in roll ups to the task's Parent Story or Escaped 
* Defect, as well as to the Epic that they belong to. This provides a means to track estimates and effort at a Story 
* and Epic level.
*
* IMPORTANT:
*
* - This script should be setup as a Script Runner Listener. (Create Script Listener, and copy and paste this script in).
* 
* - Ensure that the following listeners are associated with the script: 
*   - Issue Updated
*   - Issuelink Created
*   - Issuelink Deleted
*   - Worklog Created
*   - Worklog Deleted
*   - Worklog Updated
* 
* - Run as "ScriptRunner Add-On User"
*
*********************************************************************************************************************/


logger.info("Executing Time Roll-Up script for Webhook Event: {}", webhookEvent)

//The custom field that houses the Rough Estimate in JIRA
ROUGH_ESTIMATE_KEY = "customfield_10109" 

//The name of the JIRA field that the issueKey of the epic link that an issue is linked to
def EPIC_FIELD_LINK_NAME = "customfield_10014"

def ISSUE_UPDATED = "jira:issue_updated"
def ISSUE_LINK_CREATED = "issuelink_created";
def ISSUE_LINK_DELETED = "issuelink_deleted";

def WORK_LOG_UPDATED = "worklog_updated"
def WORK_LOG_CREATED = "worklog_created"
def WORK_LOG_DELETED = "worklog_deleted"

if (webhookEvent == ISSUE_UPDATED) {
  handleIssueUpdated();
  return;
}

if (webhookEvent == WORK_LOG_UPDATED || webhookEvent == WORK_LOG_CREATED || webhookEvent == WORK_LOG_DELETED) {
  handleWorklogUpdated();
  return;
}

if (webhookEvent == ISSUE_LINK_CREATED || webhookEvent == ISSUE_LINK_DELETED) {
  handleLinkCreatedOrDeleted();
  return;
}

//////////////////////////////////////////////////////////////////////////
// Handling Functions
//////////////////////////////////////////////////////////////////////////

//handles a worklog update
def handleWorklogUpdated(){

  //Get the issue this work log event belongs to
  def issueId = worklog.issueId;
  def result = get("/rest/api/2/issue/${issueId}").asObject(Map)
  assert result.status == 200
  def issue = result.body;
  def project = issue.fields.project.key

  //The script listener filter does not affect Work log triggers. Make sure we only make changes to projects we care about
  if (project != "TPROJ" && project != "D12N" && project != "AUTH" && project !="VTO") {
    return
  }

  def issueType = issue.fields.issuetype.name;

  if (issueType == "Task" || issueType == "WIP Defect") {
    logger.info("Handling work log event on {}, which is of type '{}'", issue.key, issueType);
    triggerStoryLevelRollUpsForIssue(issue);
  } else {
    logger.info("Not handling work log event on {}, which is of type '{}'", issue.key, issueType);
  }

}

//handles issue updated event by examining issue object
def handleIssueUpdated(){

  def project = issue.fields.project.key

  //Just in case script listener filter has not been setup 
  if (project != "TPROJ" && project != "D12N" && project != "AUTH") {
    return
  }
  //No need to make an API call, as we have the issue for these kind of events
  def issueType = issue.fields.issuetype.name;

  if (issueType == "Task" || issueType == "WIP Defect") {
    logger.info("Handling update event on {}, which is of type '{}'", issue.key, issueType);
    triggerStoryLevelRollUpsForIssue(issue);
  } else if (issueType == "User Story" || "Technical Story" || "Escaped Defect"){

    logger.info("Handling update event on {}, which is of type '{}'", issue.key, issueType);
    def roughEstimateChange = changelog?.items.find { it['fieldId'] == ROUGH_ESTIMATE_KEY}

    if(roughEstimateChange){
      logger.info("Detected change in rough estimate on issue {} - {}'", issueType, issue.key);
      result = get("/rest/api/2/issue/${issue.key}").asObject(Map)
      def parentIssue = result.body
      logger.info("Proceeding to find Epic Key for issue {}, to roll up time to Epic", issue.key)
      triggerEpicLevelRollupsForIssue(parentIssue)
      return
    }

    logger.info("Not proceeding as story update was not a Rough Estimate change");

  } else {
    logger.info("Not handling update event on {}, which is of type '{}'", issue.key, issueType);
  }
}

// handles link created or deleted by examining issue link object
def handleLinkCreatedOrDeleted(){
  logger.info("Handling issue link change event")

  def sourceIssueResponse = null;
  def parentKey = null;
  def rolledUpTime = null;
  def sourceIssueType = null;

  //Examine the issueLink object for this webhook event
  if (issueLink.issueLinkType.outwardName == "owns") {
    //The source key of in the link pair will be the Story level issue (ie: <Source> Owns <Destination> => Story Owns Task).
    //Get the source key, and perform a roll up for it
    sourceIssueResponse = get("/rest/api/2/issue/${issueLink.sourceIssueId}").asObject(Map)
    assert sourceIssueResponse.status == 200
    parentKey = sourceIssueResponse.body.key

    //Guard against the Pebcak possibility of a Task/ Wip-Defect (instead of story) being listed as owning a Task
    //Not handling this would result in erroneously summing the owned issues of the Task, and overwriting its estimate 
    sourceIssueType = sourceIssueResponse.body.fields.issuetype.name
    if (sourceIssueType != "User Story" && sourceIssueType != "Technical Story" && sourceIssueType != "Escaped Defect") {
      logger.warn("An issue other than a Story ({},) or Escaped defect was linked or unlinked as owning another issue!. Not proceeding with time roll-up", sourceIssueType)
      return;
    }

    logger.info("'Owns' Link change occured at Story/Escaped Defect level issue - {}", parentKey);
    rolledUpTime = getRolledUpTimeForStoryLevelIssue(parentKey);
    executeTimeUpdate(parentKey, rolledUpTime);

    //Now roll up the epic time. First fetch the task's parent issue as it will be needed for the Epic roll up (has the epic key)
    result = get("/rest/api/2/issue/${parentKey}").asObject(Map)
    def parentIssue = result.body
    logger.info("Proceeding to find Epic Key for issue {}, to roll up time to Epic", parentKey)
    triggerEpicLevelRollupsForIssue(parentIssue)
    return;
  }

  if (issueLink.issueLinkType.outwardName == "is Epic of") {
    //The source key of in the link pair will be the Epic that owns the story.
    //Get the source key, and perform a roll up for it
    sourceIssueResponse = get("/rest/api/2/issue/${issueLink.sourceIssueId}").asObject(Map)
    assert sourceIssueResponse.status == 200
    parentKey = sourceIssueResponse.body.key

    logger.info("Epic link change occured on issue - {}", parentKey);
    rolledUpTime = getRolledUpTimeForEpic(parentKey);
    executeTimeUpdate(parentKey, rolledUpTime);
    return;
  }

}

logger.info("Exiting Time Roll-Up script");

//////////////////////////////////////////////////////////////////////////
// Helper Functions
//////////////////////////////////////////////////////////////////////////

//Sums up the original estimates and time spent for a Parent (Story/ Escaped-Defect level) issue
def getRolledUpTimeForStoryLevelIssue(issueKey){

  //find all issues with an 'Owns' link for this issue
  def ownedIssuesJql = "issue in linkedissues(${issueKey}, 'owns')"

  def result = get('/rest/api/2/search')
    .queryString('jql', ownedIssuesJql)
    .queryString('fields', "timetracking")
    .header('Content-Type', 'application/json')
    .asObject(Map)
  assert result.status == 200

  def ownedIssues = result.body.issues
  def parentTime = sumTime(ownedIssues)
  return parentTime
}

// Given a Task Level issue, triggers a roll up to the Story level
def triggerStoryLevelRollUpsForIssue(issue){

  def linkedIssues = issue.fields.get('issuelinks')

  //Get the linked issues for the current issue (to find the parent). No need for API call, they are available in the issue body
  for (linkedIssue in linkedIssues) {

    //The inwardIssue of Owns (Owned-by) gives us the task's parent story 
    //We should really only have one Owned-by link. But in case of Pebcak errors, we'll 
    //sum up all Owned-by links for consistency's sake
    if (linkedIssue.type.name == "Owns" && linkedIssue.inwardIssue != null) {
      def parentKey = linkedIssue.inwardIssue.key;
      logger.info("Rolling up time for parent issue - {}", parentKey);
      def rolledUpTime = getRolledUpTimeForStoryLevelIssue(parentKey);

      executeTimeUpdate(parentKey, rolledUpTime);

      //Now roll up the epic time. First fetch the task's parent issue as it will be needed for the Epic roll up (has the epic key)
      result = get("/rest/api/2/issue/${parentKey}").asObject(Map)
      def parentIssue = result.body
      logger.info("Proceeding to find Epic Key for issue {}, to roll up time to Epic", parentKey)
      triggerEpicLevelRollupsForIssue(parentIssue)
    }
  }
}

// Given a Story Level issue, triggers a roll up to the Epic level
def triggerEpicLevelRollupsForIssue(issue){

  def epicKey = getIssueEpicLink(issue)

  //Can't do roll-up for epic time. Nothing left to do
  if (epicKey == null) {
    logger.info("Epic Key was null ");
    return
  }

  logger.info("Performing Epic Roll-up on {}, with rolled up time ", epicKey);
  def rolledUpTime = getRolledUpTimeForEpic(epicKey);
  executeRoughEstimateUpdate(epicKey, rolledUpTime)
  executeTimeUpdate(epicKey, rolledUpTime);
}

//Sums up the original estimates and time spent for an Epic 
def getRolledUpTimeForEpic(epicIssueKey){

  //find all issues under the Epic
  def issuesUnderEpicJql = "'Epic Link' = ${epicIssueKey}"

  def result = get('/rest/api/2/search')
    .queryString('jql', issuesUnderEpicJql)
    .queryString('fields', "customfield_10109, timetracking")
    .header('Content-Type', 'application/json')
    .asObject(Map)
  assert result.status == 200

  def issuesUnderEpic = result.body.issues
  def parentTime = sumTime(issuesUnderEpic)

  return parentTime
}

def getIssueEpicLink(issue){
  return issue.fields.customfield_10014;
}

//Updates the Rough Estimate on an issue
def executeRoughEstimateUpdate(issueKey, rolledUpTime){

   logger.info("Attempting to update issue {} with rough estimate of {}", issueKey, rolledUpTime.roughEstimateForParent)

  def result = put('/rest/api/3/issue/' + issueKey)
    .header('Content-Type', 'application/json')
    .body(
          [ "fields": 
            [(ROUGH_ESTIMATE_KEY): rolledUpTime.roughEstimateForParent] 
          ]
        )
    .asString()
}

// Updates time logged for an issue
def executeTimeUpdate(parentKey, rolledUpTime){
  
  //Fetch the issue's body
  def result = get("/rest/api/2/issue/${parentKey}").asObject(Map);
  def parentIssue = result.body;
  assert result.status == 200

  //if the time spent for the issue is 0, we cannot post a time of 0m as it results in a bad request. 
  //Instead, delete all the worklogs for the issue
  if (rolledUpTime.timeSpentForParent == 0) {
    logger.info("Deleting all worklogs for {} as timeSpent is 0", parentKey);
    deleteExistingWorklogs(parentKey);
  } else {
    updateTimeSpent(parentKey, rolledUpTime.timeSpentForParent);
  }

  updateTimeTrackingEstimates(parentKey, rolledUpTime.originalEstimateForParent, rolledUpTime.remainingEstimateForParent);
}

//Updates the original estimate and remaining estimate for an issue given the estimates in seconds
def updateTimeTrackingEstimates(issueKey, originalEstimateInSeconds, remainingEstimateInSeconds){

  def originalEstimateInMinutes = Math.round(originalEstimateInSeconds / 60);
  def remainingEstimateInMinutes = Math.round(remainingEstimateInSeconds / 60);
  logger.info("Attempting to update {}, with original estimate of {}m, and remaining estimate of {}m,", issueKey, originalEstimateInMinutes, remainingEstimateInMinutes)

  //Update Original estimate 
  def result = put('/rest/api/3/issue/' + issueKey)
    .header('Content-Type', 'application/json')
    .body([
      fields: [
        timetracking: [originalEstimate: originalEstimateInMinutes + "m",
          remainingEstimate: remainingEstimateInMinutes + "m"
        ]
      ]
    ])
    .asString()
}

//Updates the time spent (logged time) for an issue given the time spent in seconds
def updateTimeSpent(issueKey, timeSpentInSeconds){

  def timeSpentInMinutes = Math.round(timeSpentInSeconds / 60);

  //fetch the parent issue, and delete its worklog
  def result = get("/rest/api/2/issue/${issueKey}").asObject(Map)
  def numberOfWorklogsCurrently = result.body.fields.worklog.total

  //Create Worklog
  if (numberOfWorklogsCurrently == 0) {
    logger.info("Creating new time log in order to update {}, with time spent of {}m,", issueKey, timeSpentInMinutes)

    //Add timespent (logged time) (adjustEstimate = leave ensures remaining estimate is not changed)
    result = post("/rest/api/3/issue/${issueKey}/worklog")
      //adjustEstimate = leave ensures remaining estimate is not auto updated by JIRA
      .queryString('adjustEstimate', "leave")
      .header('Content-Type', 'application/json')
      .body([
        timeSpent: timeSpentInMinutes + "m"

      ])
      .asString()
    return
  }

  //Update worklog
  if (numberOfWorklogsCurrently == 1) {

    def worklog = result.body.fields.worklog.worklogs[0]

    logger.info("Updating existing time log ({}) in order to update {}, with time spent of {}m,", worklog.id, issueKey, timeSpentInMinutes)

    //Add timespent (logged time) (adjustEstimate = leave ensures remaining estimate is not changed)
    result = put("/rest/api/3/issue/${issueKey}/worklog/${worklog.id}")
      //adjustEstimate = leave ensures remaining estimate is not auto updated by JIRA
      .queryString('adjustEstimate', "leave")
      .header('Content-Type', 'application/json')
      .body([
        timeSpent: timeSpentInMinutes + "m"

      ])
      .asString()
    return
  }

  //If we have more than one log (we really shouldn't), delete them all, and post a new log
  if (numberOfWorklogsCurrently > 1) {
    deleteExistingWorklogs(issueKey)
    logger.info("Creating new time log (after deleting multiples) in order to update {}, with time spent of {}m,", issueKey, timeSpentInMinutes)

    //Add timespent (logged time) (adjustEstimate = leave ensures remaining estimate is not changed)
    result = post("/rest/api/3/issue/${issueKey}/worklog")
      //adjustEstimate = leave ensures remaining estimate is not auto updated by JIRA
      .queryString('adjustEstimate', "leave")
      .header('Content-Type', 'application/json')
      .body([
        timeSpent: timeSpentInMinutes + "m"

      ])
      .asString()
    return
  }


}

//Delete the existing worklogs on an issue
def deleteExistingWorklogs(issueKey){

  //fetch the parent issue, and delete its worklogs (hopefully just one)
  def result = get("/rest/api/2/issue/${issueKey}").asObject(Map)


  def worklogs = result.body.fields.worklog.worklogs
  for (worklog in worklogs) {
    logger.info("Deleting work log id {} from issue {}", worklog.id, issueKey)

    result = delete ('/rest/api/3/issue/' + issueKey + '/worklog/' + worklog.id)
      //adjustEstimate = leave ensures remaining estimate is not auto updated by JIRA
      .queryString('adjustEstimate', "leave")
      .header('Content-Type', 'application/json')
      .asObject(Map)
  }

}

//Returns sum of estimates and time spent for passed-in issues
def sumTime(issues){
    
  def originalEstimateTotal = 0;
  def remainingEstimateTotal = 0;
  def timeSpentTotal = 0;
  def roughEstimateTotal = 0;

  // Sum up the times
  for (issue in issues) {

    def timeTrackingInfo = issue.fields.timetracking;

    def originalEstimateForIssue = timeTrackingInfo.originalEstimateSeconds
    originalEstimateForIssue = (originalEstimateForIssue == null) ? 0 : originalEstimateForIssue
    originalEstimateTotal = originalEstimateTotal + originalEstimateForIssue

    def remainingEstimateForIssue = timeTrackingInfo.remainingEstimateSeconds
    remainingEstimateForIssue = (remainingEstimateForIssue == null) ? 0 : remainingEstimateForIssue
    remainingEstimateTotal = remainingEstimateTotal + remainingEstimateForIssue

    def timeSpentForIssue = timeTrackingInfo.timeSpentSeconds
    timeSpentForIssue = (timeSpentForIssue == null) ? 0 : timeSpentForIssue
    timeSpentTotal = timeSpentTotal + timeSpentForIssue
    
    def roughEstimateForIssue = issue.fields[ROUGH_ESTIMATE_KEY]
    roughEstimateForIssue = (roughEstimateForIssue == null) ? 0 : roughEstimateForIssue
    roughEstimateTotal = roughEstimateTotal + roughEstimateForIssue
  }

  def parentTime = [originalEstimateForParent: originalEstimateTotal,
    remainingEstimateForParent : remainingEstimateTotal,
    timeSpentForParent : timeSpentTotal,
    roughEstimateForParent: roughEstimateTotal]

  return parentTime
}
