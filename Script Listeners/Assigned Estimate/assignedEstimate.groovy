/*********************************************************************************************************************
* assignedEstimate.groovy
* v1.0
*
* ScriptRunner Type: Script Listener
*
* This script monitors Tasks and WIP-Defects for an Original Estimate. If set, the Task will be transitioned to 'Ready',
* if it is currently in a 'Pending' state.  
*
* This script also monitors stories. If they receive a ROM, they will transition to 'Ready for Planning'
*
* IMPORTANT:
*
* - This script should be setup as a Script Runner Listener. (Create Script Listener, and copy and paste this script in).
* 
* - Ensure that the following listeners are associated with the script: 
*   - Issue Updated
*   - Issue Created
* 
* - Run as "ScriptRunner Add-On User"
*********************************************************************************************************************/

def ISSUE_UPDATED = "jira:issue_updated"
def ISSUE_CREATED = "jira:issue_created"

// Projects this script should run on
def project = issue.fields.project.key
if (project != "TPROJ" && project != "D12N" && project != "AUTH" && project !="VTO") {
  return
}

def issueType = issue.fields.issuetype.name;

if (webhookEvent == ISSUE_UPDATED) {

  if (issueType == "Task" || issueType == "WIP Defect") {
     handleTaskLevelUpdate()
     return
  }  

  if (issueType == "User Story" || issueType == "Technical Story") {
     handleStoryLevelUpdate()
     return
  }  

}

if (webhookEvent == ISSUE_CREATED) {

  if (issueType == "Task" || issueType == "WIP Defect") {
     handleTaskLevelCreate()
     return
  }  

}

def handleTaskLevelCreate(){  
  def PENDING_TO_READY_TRANSITION_ID = 11
  def originalEstimate = issue.fields.timeoriginalestimate

  if(originalEstimate != null && originalEstimate !=0){
    handleTransition(PENDING_TO_READY_TRANSITION_ID)
  }
}

def handleTaskLevelUpdate(){  
  //The Id of the Pending -> Ready transition in the Task / WIP-Defect workflow
  def PENDING_TO_READY_TRANSITION_ID = 11

  def timeOriginalEstimateChange = changelog?.items.find { it['field'] == 'timeoriginalestimate' }
  if(timeOriginalEstimateChange) {
  
    def fromEstimate = timeOriginalEstimateChange.fromString
    def toEstimate = timeOriginalEstimateChange.toString

      if(toEstimate == null || toEstimate == "0"){
      //Not a valid value to consider Estimated
      return
    }

    logger.info("Executing Assigned Estimate Script on Task-level issue: {}", webhookEvent)
    logger.info("Time tracking changed from {} to {}", fromEstimate, toEstimate)

    //Task is estimated. Transition to Estimated if the task is in a Pending state
    def currentStatus = issue.fields.status.name;
    if(currentStatus!="Pending"){
      logger.info("Task is estimated, but not in Pending state. Not able to transition")
      return
    }

    logger.info("Attempting to transition Task {} from 'Pending' to 'Ready'", issue.key)
    handleTransition(PENDING_TO_READY_TRANSITION_ID)
  }
}

def handleStoryLevelUpdate(){

  def roughEstimateChange = changelog?.items.find { it['field'] == 'Rough Estimate' }
  def timeOriginalEstimateChange = changelog?.items.find { it['field'] == 'timeoriginalestimate' }
  
  if(roughEstimateChange) {
    handleStoryROMUpdate(roughEstimateChange)
  } 

  if(timeOriginalEstimateChange) {
    handleStoryOriginalEstimateUpdate(timeOriginalEstimateChange)
  } 

}

def handleStoryOriginalEstimateUpdate(timeOriginalEstimateChange){

  //The Id of the 'Ready for Planning' -> Ready transition in the Story workflow
  def READY_FOR_PLANNING_TO_READY_TRANSITION_ID = 191
    
  def fromEstimate = timeOriginalEstimateChange.fromString
  def toEstimate = timeOriginalEstimateChange.toString

  if(toEstimate == null || toEstimate == "0"){
    //Not a valid value to consider Estimated
    return
  }

  logger.info("Executing Assigned Estimate Script on Story-level issue: {}", webhookEvent)
  logger.info("Time tracking changed from {} to {}", fromEstimate, toEstimate)

  //Story has an original estimate. Transition to Ready if the task is in 'Ready for Planning' state
  def currentStatus = issue.fields.status.name;
  if(currentStatus!="Ready For Planning"){
    logger.info("Story has an original estimate, but is not in 'Ready for Planning' state. Not able to transition")
    return
  }

  logger.info("Attempting to transition Story {} from 'Ready for Planning' to 'Ready'", issue.key)
  handleTransition(PENDING_TO_READY_TRANSITION_ID)
}


def handleStoryROMUpdate(roughEstimateChange){
  
    //The Id of the Submitted -> Ready for Planning transition in the Story workflow
    def SUBMITTED_TO_READY_FOR_PLANNING_TRANSITION_ID = 61

    //The Id of the Ready for Planning -> Submitted transition in the Story workflow
    def READY_FOR_PLANNING_TO_SUBMITTED_TRANSITION_ID = 141

    def fromEstimate = roughEstimateChange.fromString
    def toEstimate = roughEstimateChange.toString

    def currentStatus = issue.fields.status.name;

    if(toEstimate == null || toEstimate == "0" || toEstimate == ""){
      //If the story is in a 'Ready For Planning' state, and the estimate is changed
      //to one of these values, we'll assume that the ROM has been cleared, and this 
      //story needs to go back to a Submitted State. For any other state, we'll 
      //consider this an invalid value, and won't action it
      if(currentStatus=="Ready For Planning"){
        logger.info("Rough Estimate cleared for issue {}. Transitioning from 'Ready for Planning' to 'Submitted'", issue.key)
        handleTransition(READY_FOR_PLANNING_TO_SUBMITTED_TRANSITION_ID)
      } else{
        logger.info("Rough Estimate cleared for issue {}. Not in 'Ready for Planning' state. Won't action.", issue.key)
      }
      return
    }

    logger.info("Executing Assigned Estimate Script on Story-level issue: {}", webhookEvent)
    logger.info("Rough Estimate changed from {} to {}", fromEstimate, toEstimate)

    //Story is estimated. Transition to 'Ready for Planning' if the story is in 'Submitted' state
    if(currentStatus!="Submitted"){
      logger.info("Story has Rough Estimate, but not in 'Submitted' state. Not able to transition")
      return
    }

    logger.info("Attempting to transition {} from 'Submitted' to 'Ready for Planning'", issue.key)
    handleTransition(SUBMITTED_TO_READY_FOR_PLANNING_TRANSITION_ID)
}

def handleTransition(transitionId){

    //The key of the issue we are currently working on 
    def issueKey = issue.key

    def  result = post("/rest/api/3/issue/${issueKey}/transitions")
        .header('Content-Type', 'application/json')
        .body([
          transition: [
            id : transitionId
          ]
        ])
        .asString()

      if (result.status == 204) {
        logger.info("Successfully transitioned issue")
        return
      }
}
